output "vpc" {
  description = "VPC name."
  value       = "${local.name}-VPC"
}

output "route_table" {
  description = "Route table name."
  value       = local.name
}

output "subnet" {
  description = "Subnet name."
  value       = local.name
}

output "flow_log" {
  description = "Flow log name."
  value       = "flowlog-${local.name}"
}

output "lambda_log_group" {
  description = "Lambda log group name."
  value       = "lambda-${local.name}"
}

output "iam_role_name" {
  description = "IAM role name."
  value       = local.name
}

output "account" {
  description = "Account name."
  value       = local.name
}

output "s3_bucket" {
  description = "S3 bucket name."
  value       = local.name
}

output "security_group" {
  description = "Security group name."
  value       = local.name
}

output "account_alias" {
  description = "Account alias name."
  value       = local.name
}

output "dynamodb_table" {
  description = "DynamoDb table name."
  value       = local.name
}

output "kms_key" {
  description = "KMS key name."
  value       = local.name
}

output "switch_role_name" {
  description = "Switch role name."
  value       = local.name
}

output "iam_role_policy_name" {
  description = "IAM Role policy name."
  value       = local.name
}

output "iam_policy_name" {
  description = "IAM policy name."
  value       = local.name
}

output "config_configuration_recorder" {
  description = "AWS Config configuration recorder name."
  value       = local.name
}

output "vpc_endpoint_s3" {
  description = "VPC endpoint for S3 name."
  value       = local.name
}

output "vpc_endpoint_kms" {
  description = "VPC endpoint for KMS name."
  value       = local.name
}

/*output "hosted_zone_private" {
  description = "Private Hosted Zone name."
  value       = "${var.stack}.${var.project}.${var.env}"
}*/

output "ram_resource_share" {
  description = "RAM Resource Share name."
  value       = "ram-rs-${local.name}"
}

output "tgw_vpc_attachment" {
  description = "Transit Gateway VPC attachment name."
  value       = "att-${local.name}"
}

output "lambda" {
  description = "Lambda function name."
  value       = local.name
}

output "tgw_route_table" {
  description = "Transit Gateway route table name."
  value       = "tgw-rt-${local.name}"
}

output "tgw" {
  value = "tgw-${local.name}"
}

output "eip" {
  value = local.name
}

output "nat" {
  value = local.name
}