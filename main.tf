data "aws_availability_zones" "all" {}

module "inspection-vpc" {

  source = "./terraform-module-aws-inspection-vpc/networking"

  env     = "T"
  region  = "UW2"
  bu      = "GP"
  project = "IVPC"

  azs = [data.aws_availability_zones.all.names[0], data.aws_availability_zones.all.names[1]]

  vpc_cidr_block = "10.128.0.0/23"

  private_subnets      = 6
  private_subnets_cidr = ["10.128.0.0/27", "10.128.1.0/27", "10.128.0.64/28", "10.128.1.64/28", "10.128.0.80/28", "10.128.1.80/28"]

  public_subnets      = 2
  public_subnets_cidr = ["10.128.0.32/27", "10.128.1.32/27"]


}