output "vpc_id" {
  description = "ID of the VPC."
  value       = aws_vpc.vpc.id
}

output "vpc_cidr" {
  description = "CIDR block of the VPC."
  value       = aws_vpc.vpc.cidr_block
}

output "private_subnet_id" {
  description = "ID of the private subnet/s."
  value       = aws_subnet.data.*.id
}

output "public_subnet_id" {
  description = "ID of the public subnet/s."
  value       = aws_subnet.natgw.*.id
}

