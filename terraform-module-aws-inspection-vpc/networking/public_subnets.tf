resource "aws_subnet" "natgw" {
  count = var.public_subnets 

  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.public_subnets_cidr[count.index]
  availability_zone  = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null

  tags = merge(local.tags, tomap({ "Name" = "${module.naming.subnet}-SNET-${count.index}" }))

  
}

resource "aws_route_table" "natgw" {
  count = var.public_subnets

  vpc_id = aws_vpc.vpc.id

  tags = merge(local.tags, tomap({ "Name" = "${module.naming.route_table}-RTB-${count.index}" }))

  
}

resource "aws_route_table_association" "natgw" {
  count = var.public_subnets

  route_table_id = element(aws_route_table.natgw.*.id, count.index)
  subnet_id      = element(aws_subnet.natgw.*.id, count.index)
}

resource "aws_eip" "natgw" {
  count = var.public_subnets

  vpc = true

  tags = merge(local.tags, tomap({ "Name" = "${module.naming.eip}-EIP-${count.index}" }))
}

resource "aws_nat_gateway" "natgw" {
  count = var.public_subnets

  allocation_id = element(aws_eip.natgw.*.id, count.index)
  subnet_id     = element(aws_subnet.natgw.*.id, count.index)

  tags = merge(local.tags, tomap({ "Name" = "${module.naming.nat}-NATGW-${count.index}" }))


}