locals {
  module_tags = {
    module  = "aws-inspection-vpc"
    env     = var.env
    region  = var.region
    bu      = var.bu
    project = var.project
  }

  name   = "complete-example"
  region = "us-east-2"
  tags = merge(local.module_tags, var.extra_tags)
}

module "naming" {
  source = "../../terraform-module-aws-naming"

  env     = var.env
  region  = var.region
  bu      = var.bu
  project = var.project
}
