resource "aws_subnet" "data" {
  count = var.private_subnets

  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.private_subnets_cidr[count.index]
  availability_zone  = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null

  tags = merge(local.tags, tomap({ "Name" = "${module.naming.subnet}-SNET-${count.index}" }))

  
}

resource "aws_route_table" "data" {
  count = var.private_subnets

  vpc_id = aws_vpc.vpc.id

  tags = merge(local.tags, tomap({ "Name" = "${module.naming.subnet}-RTB-${count.index}" }))

  

}

resource "aws_route_table_association" "data" {
  count = var.private_subnets

  route_table_id = element(aws_route_table.data.*.id, count.index)
  subnet_id      = element(aws_subnet.data.*.id, count.index)
}
