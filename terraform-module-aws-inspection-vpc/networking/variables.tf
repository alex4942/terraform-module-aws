variable "env" {
  description = "code of the environment."
}

variable "region" {
  description = "code of the region."
}

variable "bu" {
  description = "code of the business unit."
}

variable "project" {
  description = "Name of the account project."
}

variable "extra_tags" {
  description = "Set of extra tags."
  default     = {}
}

variable "azs" {
  description = "A list of availability zones names or ids in the region"
  type        = list(string)
  default     = []
}

variable "private_subnets" {
  description = "Number of private subnets to be deployed."
}

variable "private_subnets_cidr" {
  description = "CIDR block for private subnets"
}

variable "public_subnets" {
  description = "Number of public subnets to be deployed."
}

variable "public_subnets_cidr" {
  description = "CIDR block for public subnets"
}

variable "vpc_cidr_block" {
  description = "CIDR block for the baseline VPC (including mask)."
}

