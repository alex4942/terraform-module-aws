
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags                 = merge(local.tags, tomap({ "Name" = module.naming.vpc }))
  
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
}
